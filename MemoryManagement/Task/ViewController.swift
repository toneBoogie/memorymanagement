//
//  ViewController.swift
//  MemoryManagement
//
//  Created by THXDBase on 10.02.2021.
//  Copyright © 2021 MOBILESTYLES. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var man: Person?
        var woman: Person?
        var son: Person?
        var sonWife: Person?
        var grandSon: Person?
        var grandDaughter: Person?
        
        man = Person(name: "John",
                     surname: "Connor",
                     birthDate: Date(),
                     mother: nil,
                     father: nil) { (manPerson) -> [Person] in
                        
                        woman = Person(name: "Jane",
                                       surname: "Brien",
                                       birthDate: Date(),
                                       mother: nil,
                                       father: nil,
                                       childrenClosure: { (womanPerson) -> [Person] in
                                        
                                        son = Person(name: "Nikita",
                                                     surname: "Connor",
                                                     birthDate: Date(),
                                                     mother: womanPerson,
                                                     father: manPerson,
                                                     childrenClosure: { (sonPerson) -> [Person] in
                                                        
                                                        sonWife = Person(name: "Natasha", surname: "Karenina", birthDate: Date(), mother: nil, father: nil, childrenClosure: { (sonWifePerson) -> [Person] in
                                                           
                                                            grandSon = Person(name: "Pawel",
                                                                              surname: "Connor",
                                                                              birthDate: Date(),
                                                                              mother: sonWifePerson,
                                                                              father: sonPerson,
                                                                              childrenClosure: { (selfPerson) -> [Person] in
                                                                                return []
                                                            })
                                                            
                                                            grandDaughter = Person(name: "Polina",
                                                                                   surname: "Connor",
                                                                                   birthDate: Date(),
                                                                                   mother: sonWifePerson,
                                                                                   father: sonPerson,
                                                                                   childrenClosure: { (selfPerson) -> [Person] in
                                                                                    return []
                                                            })
                                                            
                                                            if let grandSon = grandSon, let grandDaughter = grandDaughter {
                                                                return [grandSon, grandDaughter]
                                                            }
                                                            return []
                                                        })
                                                        
                                                        if let grandSon = grandSon, let grandDaughter = grandDaughter {
                                                            return [grandSon, grandDaughter]
                                                        }
                                                        return []
                                        })
                                        if let son = son {
                                            return [son]
                                        }
                                        return []
                        })
                        
                        if let son = son {
                            return [son]
                        }
                        return []
        }
        
        
        print(man?.getChildrenNames() ?? "")
        print(woman?.getChildrenNames() ?? "")
        print(son?.getParentsNames() ?? "")
        print(son?.getChildrenNames() ?? "")
        print(sonWife?.getChildrenNames() ?? "")
        print(grandSon?.getParentsNames() ?? "")
        print(grandDaughter?.getParentsNames() ?? "")
    }
    
    
}

