//
//  Person.swift
//  MemoryManagement
//
//  Created by THXDBase on 10.02.2021.
//  Copyright © 2021 MOBILESTYLES. All rights reserved.
//

import Foundation

class Person {
    private let name: String
    private let surname: String
    private let birthDate: Date
    weak private var mother: Person?
    weak private var father: Person?
    private var children: [Person] = []
    var fullName: String {
        return name.appending(" ").appending(surname)
    }
    var age: Int {
        return 0
    }
    
    init(name: String,
         surname: String,
         birthDate: Date,
         mother: Person?,
         father: Person?,
         childrenClosure: (Person) -> [Person]) {
        
        self.name = name
        self.surname = surname
        self.birthDate = birthDate
        self.mother = mother
        self.father = father
        self.children = childrenClosure(self)
    }
    
    init() {
        self.name = "name"
        self.surname = "surname"
        self.birthDate = Date()
        self.mother = nil
        self.father = nil
        self.children = []
    }

    func getParentsNames() -> String {
        return "\(fullName) father: \(father?.fullName ?? ""), mother: \(mother?.fullName ?? "") "
    }
    func getChildrenNames() -> String {
        let childrenNames = children.map({ $0.fullName })
        return "\(fullName) children: \(childrenNames)"
    }
    
    deinit {
        print("\(name) deallocated")
    }
}
